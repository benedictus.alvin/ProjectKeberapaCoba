$(document).ready(function() {
    $(".chat-text").keypress(function(e) {
    
    if(e.which == 13) {
    	e.preventDefault(); 
    	var input = $("textarea").val();
      $("textarea").val(""); 
      $(".msg-insert").append('<p class="msg-send">'+input+'</p>'+'<br/>'); 
    }
	
	});

    $(".chat-button").click(function(){
      var input = $("textarea").val(); 
      $("textarea").val(""); 
        $(".msg-insert").append('<p class="msg-send">'+input+'</p>'+'<br/>'); 
    });

  
  var themes = [{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
	];
  var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};
  
  
  if (localStorage.getItem("selectedTheme") === null) {
    localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
  }
  
  localStorage.setItem('themes', JSON.stringify(themes));
	
  
	var retrievedObject = localStorage.getItem('themes');
	$('.my-select').select2({data: themes});
	

    var retrievedSelected = JSON.parse(localStorage.getItem('selectedTheme'));
    var key;
    var bcgColor;
    var fontColor;
	for (key in retrievedSelected) {
    	if (retrievedSelected.hasOwnProperty(key)) {
        	bcgColor=retrievedSelected[key].bcgColor;
          fontColor=retrievedSelected[key].fontColor;
    	}
	}  
	$("body").css({"background-color": bcgColor});
	$("footer").css({"color":fontColor});


	$('.apply-button').on('click', function(){  
    
    var valueTheme = $('.my-select').val();
    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
    // [TODO] ambil object theme yang dipilih
    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
    // [TODO] simpan object theme tadi ke local storage selectedTheme
    var theme;
    var a;
    var selectedTheme = {};

    for(a in themes){
    	if(a==valueTheme){
    		var bcgColor = themes[a].bcgColor;
    		var fontColor = themes[a].fontColor;
    		var text = themes[a].text;
    		$("body").css({"background-color": bcgColor});
			  $("footer").css({"color":fontColor});
    		selectedTheme[text] = {"bcgColor":bcgColor,"fontColor":fontColor};
    		localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
    	}
    }
});
});

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = '';
    erase = true;
  } else if (x === 'eval') {
  		var res;
		try {
			res = Math.round(evil(print.value) * 10000) / 10000;
		} catch(SyntaxError) {
			res = "Syntax Error";
		}
		if(res == Infinity) res = "Undefined";
		print.value = res;
		erase = true;
  } else if (x === 'log') {
  		print.value = Math.log10(print.value);
  } else if (x === 'sin') {
  		print.value = Math.sin(print.value * Math.PI / 180); //in degrees
  		if(print.value == 1.2246467991473532e-16) print.value = 0;
  } else if (x === 'tan') {
  		print.value = Math.tan(print.value * Math.PI / 180); //in degrees
  		if(print.value == -1.2246467991473532e-16) print.value = 0;
  		else if(print.value == 0.9999999999999999) print.value = 1;
  } else {
  		if(x===0 && print.value==='') print.value = '';
		else print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END